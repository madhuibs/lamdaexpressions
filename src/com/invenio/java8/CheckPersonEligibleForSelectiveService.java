package com.invenio.java8;

public class CheckPersonEligibleForSelectiveService implements CheckPerson {

	@Override
	public boolean test(Person p) {
		return p.gender == Sex.MALE &&
	            p.getAge() >= 18 &&
	            p.getAge() <= 25;
	}

}
