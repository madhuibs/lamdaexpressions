package com.invenio.java8;

public interface CheckPerson {

	public boolean test(Person p) ;

}
