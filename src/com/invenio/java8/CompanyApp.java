package com.invenio.java8;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class CompanyApp {

	// Example1 :  Create Methods That Search for Members That Match One
	// Characteristic
	public static void printPersonsOlderThan(List<Person> roster, int age) {
		for (Person p : roster) {
			if (p.getAge() >= age) {
				p.printPerson();
			}
		}
	}
	
	// Example2 :  Create Methods That Search for Members within a give age range.
	public static void printPersonsWithinAgeRange(
		    List<Person> roster, int low, int high) {
		    for (Person p : roster) {
		        if (low <= p.getAge() && p.getAge() < high) {
		            p.printPerson();
		        }
		    }
		}
	
	//Example3: Specify Search Criteria Code in a Local Class
	
	public static void printPersons(  List<Person> roster, CheckPerson tester) {
		    for (Person p : roster) {
		        if (tester.test(p)) {
		            p.printPerson();
		        }
		    }
		}
	
	//Example 6: Use Standard Functional Interfaces with Lambda Expressions
	
	/*
		interface Predicate<T> {
		    boolean test(T t);
		}
	  
	 */
			
	public static void printPersonsWithPredicate(
		    List<Person> roster, Predicate<Person> tester) {
		    for (Person p : roster) {
		        if (tester.test(p)) {
		            p.printPerson();
		        }
		    }
		}
	
	//Example 7: use lambda expressions , do more operations on the Person
	
	public static void processPersons(
		    List<Person> roster,
		    Predicate<Person> tester,
		    Consumer<Person> block) {
		        for (Person p : roster) {
		            if (tester.test(p)) {
		                block.accept(p);
		            }
		        }
		}
	
	//Example 8: what If you want to validate the members' profiles or retrieve their contact information?
	
	public static void processPersonsWithFunction(
		    List<Person> roster,
		    Predicate<Person> tester,
		    Function<Person, String> mapper,
		    Consumer<String> block) {
		    for (Person p : roster) {
		        if (tester.test(p)) {
		            String data = mapper.apply(p);
		            block.accept(data);
		        }
		    }
		}

	public static void main(String[] args) {
		
		List<Person> roster = getRoster();
		System.err.println("persons roster: "+roster);
		
		
		//test3: Specify Search Criteria Code in a Local Class
		printPersons( roster , new CheckPersonEligibleForSelectiveService());
		
		System.out.println("---------------------------------");
		
		//test4 Example4: Specify Search Criteria Code in an Anonymous Class
		printPersons(
			    roster,
			    new CheckPerson() {
			        public boolean test(Person p) {
			            return p.getGender() == Sex.MALE
			                && p.getAge() >= 18
			                && p.getAge() <= 25;
			        }
			    }
			);
		
		System.out.println("---------------------------------");
		//Test Example 5: Specify Search Criteria Code with a Lambda Expression
		printPersons(
			    roster,
			    (Person p) -> p.getGender() == Sex.MALE
			        && p.getAge() >= 18
			        && p.getAge() <= 25
			);
		
		System.out.println("---------------------------------");
		//Test example 6:
		printPersonsWithPredicate(
			    roster,
			    p -> p.getGender() == Sex.MALE
			        && p.getAge() >= 18
			        && p.getAge() <= 25
			);
		System.out.println("---------------------------------");
		//Test example 7:
		
		processPersons(
			     roster,
			     p -> p.getGender() == Sex.MALE
			         && p.getAge() >= 18
			         && p.getAge() <= 25,
			     p -> p.printPerson()
			);
		System.out.println("---------------------------------");
		//Test Example 8:
		
		processPersonsWithFunction(
			    roster,
			    p -> p.getGender() == Sex.MALE
			        && p.getAge() >= 18
			        && p.getAge() <= 25,
			    p -> p.getEmail(),
			    email -> System.out.println(email)
			);
		System.out.println("---------------------------------");
		
		//Test Example 9: Use Aggregate Operations That Accept Lambda Expressions as Parameters
		roster
	    .stream()
	    .filter(
	        p -> p.getGender() == Sex.MALE
	            && p.getAge() >= 18
	            && p.getAge() <= 25)
	    .map(p -> p.getEmail())
	    .forEach(email -> System.out.println(email));


	}
	
	
	/*
	 * Sample data
	 */
	private static List<Person> getRoster() {
		List<Person> roster = new ArrayList<>();
		IntStream.range(1, 5).forEach(i -> {
			Person p = new Person();
			p.setName("Madhu"+i);
			p.setBirthDay(LocalDate.of(1990+(i*2), 1, 1));
			p.setEmail("test"+i+"@invenio.com");
			p.setGender(Sex.MALE);
			roster.add(p);
		});
		return roster;
	}

}
