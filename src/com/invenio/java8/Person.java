package com.invenio.java8;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Person {
	public String name;
	public LocalDate birthDay;
	public String email;
	public Sex gender;
	
	
	 public void printPerson() {
	       System.out.println(name+" : "+birthDay);
	    }
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(LocalDate birthDay) {
		this.birthDay = birthDay;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public long getAge() {
		return this.birthDay.until(LocalDate.now(), ChronoUnit.YEARS);
	}


	public Sex getGender() {
		return gender;
	}


	public void setGender(Sex gender) {
		this.gender = gender;
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", birthDay=" + birthDay + ", email=" + email + ", gender=" + gender + "]";
	}


	
	

}
